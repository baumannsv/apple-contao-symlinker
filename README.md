Contao Symlink for Apple
========================

This generate Symlinks from Extension to the Contao Project


System requirements
-------------------

 * Contao 3.2
 * Max OSX


Installation
------------

Compile this as App with Apple Script


Documentation
-------------

 The folder hierarchy from Extension  must be same as the Contao Project

 * add Symlink from Extension
 	1. choose the addSymlink Mode
 	2. choose the Extension
 	3. choose the Contao Project


* remove Symlink from Contao Project
 	1. choose the removeSymlink Mode
 	2. choose the Extension
 	3. choose the Contao Project

License
-------

Contao Symlink for Apple is licensed under the terms of the LGPLv3. The full license text is
available in the [`system/docs`][7] folder from Cotnao CMS.

Note that the LGPL incorporates the terms and conditions of the GPL, therefore
both licenses are included there. This, however, does not imply that Contao is
dual licensed under both the GPL and the LGPL.


Getting support
---------------

baumann.sv@gmail.com

[7]: system/docs
